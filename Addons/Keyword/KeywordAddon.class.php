<?php

namespace Addons\Keyword;
use Common\Controller\Addon;
use Think\Db;

/**
 * 路由插件
 * Class RouteAddon
 * @package Addons\Route
 * @author Mr.Li<lhb2002@qq.com>
 */
class KeywordAddon extends Addon {
    public $info = array(
        'name' => 'Keyword',
        'title' => 'SEO优化',
        'description' => 'title，keyword，description设置 ',
        'status' => 1,
        'author' => 'Mr.Li<lhb2002@qq.com>',
        'version' => '0.1'
    );
    public $addon_path = './Addons/Keyword/';

    private function table_name(){
        $db_prefix = C('DB_PREFIX');
        $table_name = "{$db_prefix}Seo";
        return $table_name;
    }
    public function install()
    {
        $model = D();
        $db_prefix = C('DB_PREFIX');
        $bools = $model -> execute("INSERT INTO `{$db_prefix}hooks`(`name`,`description`,`type`)VALUES('view_meta','页面SEO信息','1'),('admin_edit_view','后台编辑SEO信息','1'),('admin_edit_save','后台保存SEO信息','1');");
        if ($bools < 1) {
            session('addons_install_error', '在线留言提交钩子添加失败');
            return false;
        }
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `{$this->table_name()}` (
  `id` int(11) NOT NULL COMMENT '主键',
  `mode` varchar(255) NOT NULL COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `keyword` varchar(255) DEFAULT NULL COMMENT '关键词',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `create_time` int(10) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`,`mode`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='SEO';
SQL;
        $model -> execute($sql);
        if (count($model -> query("SHOW TABLES LIKE '" . $this -> table_name() . "'")) != 1) {
            session('addons_install_error', ',Message表未创建成功，请手动检查插件中的sql，修复后重新安装' . $sql);
            return false;
        }
        return true;
    }

    public function uninstall()
    {
        $db_prefix = C('DB_PREFIX');
        $model = D();
        $sql = "DROP TABLE IF EXISTS `" . $this -> table_name() . "`;";
        $model -> execute($sql);
        $sql = "DELETE FROM `{$db_prefix}hooks` WHERE `name` in ('view_meta','admin_edit_view','admin_edit_save')";
        $model -> execute($sql);
        return true;
    }

    /**
     * 根据页面参数，加载seo信息
     * @param $parameter
     * @author Mr.Li<lhb2002@qq.com>
     *        {:hook('view_meta',array('id'=>$info['id'],'category'=>$category['id']))}
     */
    public function view_meta(&$parameter){
        $seo_db = D('Addons://Keyword/Seo');
        if($parameter['id']){   //加载文档seo信息
            $article_seo = $seo_db->detail($parameter['id'],'article');
        }
        if($parameter['category']) {    //加载分类seo信息
            $category_seo = $seo_db->detail($parameter['category'], 'category');
        }

        ($title = $article_seo['title']) || ($title = $category_seo['title']) || ($title = C('WEB_SITE_TITLE'));

        ($keyword = $article_seo['keyword']) || ($keyword = $category_seo['keyword']) || ($keyword = C('WEB_SITE_KEYWORDS'));
        ($description = $article_seo['description']) || ($description = $category_seo['description']) || ($description = C('WEB_SITE_DESCRIPTION'));

        echo "<title>{$title}</title>";
        echo "<meta name=\"keyword\" content=\"{$keyword}\">";
        echo "<meta name=\"description\" content=\"{$description}\">";
    }

    /**
     * 后台编辑关键词插件
     * @param string $mode
     * @param int $id
     * @author Mr.Li<lhb2002@qq.com>
     * 修改 文章编辑
     *      Application/Admin/View/Article/add.html
     *       line 193 增加       {:hook('admin_edit_view',array('mode'=>'article'))}
     *      Application/Admin/View/Article/edit.html
     *       line 212 增加       {:hook('admin_edit_view',array('mode'=>'article','id'=>))}
     *
     */
    public function admin_edit_view($parameter){
        if($parameter['id']){
            $seo = D('Addons://Keyword/Seo')->detail($parameter['id'],$parameter['mode']);
            if($seo){
                $parameter = $seo;
            }
        }

        $this->assign('seo',$parameter);
        $this->display('seo');
    }

    /**
     * 保存关键词信息
     * @param $data
     * @return mixed
     * @author Mr.Li<lhb2002@qq.com>
     */
    public function admin_edit_save($data){
        \Think\Log::write(json_encode($data));
        $seo_db = D('Addons://Keyword/Seo');
        $status =  $seo_db->update($data);
        \Think\Log::write($seo_db->getError());
        return $status;
    }

}
