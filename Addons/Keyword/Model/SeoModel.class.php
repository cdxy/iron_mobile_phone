<?php

namespace Addons\Keyword\Model;
use Think\Model;

class SeoModel extends Model
{
    protected $pk = array('id','mode');
	/* 自动完成规则 */
	protected $_auto = array(
	    array('create_time',NOW_TIME,self::MODEL_BOTH),
    );

	/**
	 * 新增或更新一个文档
	 * @return boolean fasle 失败 ， int  成功 返回完整的数据
	 */
	public function update($data=array()) {
		/* 获取数据对象 */
		$data = $this -> create($data);
		if (empty($data)) {
			return false;
		}
		/* 添加或新增基础内容 */
		$map = array(
		    'id'=>$data['id'],
            'mode'=>$data['mode']
        );
		$info = $this->where($map)->find();
		if ($info) {//更新数据
            $status = $this ->where($map)-> save($data);
            //更新基础内容
            if (false === $status) {
                return false;
            }
		} else {//新增数据
            $id = $this -> add();
            //添加基础内容
            if (!$id) {
                return false;
            }
		}
		//内容添加或更新完成
		return $data;
	}

	public function detail($id,$mode = 'article'){
	    $map = array(
            'id'=>$id,
            'mode'=>$mode
        );
        return $this->where($map)->find();
    }


}
