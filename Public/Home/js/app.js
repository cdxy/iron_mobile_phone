require.config({
    baseUrl:"/Public/Home/js",
    paths:{
        "jquery":"../../static/jquery-2.0.3.min",
        "mui":"../../static/mui/js/mui.min",
        "mui_picker":"../../static/mui/plugin/picker/dist/js/mui.picker.min"
    },
    map:{
        "*":{
            "css":"../../static/require-css/css.min"
        }
    },
    shim:{
        "jquery":{
            exports: '$'
        },
        "mui":{
            deps:[
                //"css!../../static/mui/css/mui.css"
            ]
        },
        "mui_picker":{
            deps:[
                "mui",
                "css!../../static/mui/plugin/picker/dist/css/mui.picker.min.css"
            ]
        }
    }
});

require(['jquery','page','mui'],function ($,Page,Mui) {
    var Config = requirejs.s.contexts._.config.config;
    //todo 标准开发时，可以不使用load方法加载头部和未部
    //Page.load();
    //Mui.init();
    if (Config.jsname) {
        require([Config.jsname], function (Controller) {
            Controller[Config.actionname] != undefined && Controller[Config.actionname]();
        }, function (e) {
            console.error(e);
            // 这里可捕获模块加载的错误
        });
    }


});