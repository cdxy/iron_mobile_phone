define(["jquery","mui","mui_picker"], function($,Mui,picker) {
    var Pagination = {
        /*获取地址*/
        getUrl : function (url,page) {
            return url.replace('__PAGE__',page);
        },
        /*上一页*/
        prive:function (opts) {
            var p = opts.page<2 ? 1 :opts.page-1;
            return $("<a/>").text('上一页').attr('href',Pagination.getUrl(opts.url,p));
        },
        /*下一页*/
        next:function (opts) {
            var p = opts.page<opts.pages ? opts.page+1 :opts.pages;
            return $("<a/>").text('下一页').attr('href',Pagination.getUrl(opts.url,p));
        },
        select:function (opts) {
            var data = [];
            for( var i = 0 ; i < opts.pages ; i++ ){
                var page = i + 1;
                data.push({value:page,text:page+"/"+opts.pages});
            }
            var picker = new Mui.PopPicker();
            picker.setData(data);
            picker.pickers[0].setSelectedValue(opts.page, 2000);

            var $select = $("<div class='page-number'><span class='current'></span>/<span class='total'></span></div>");
            $(".current",$select).text(opts.page);
            $(".total",$select).text(opts.pages);

            $select.on("tap click",function () {
               picker.show(function(SelectedItem){
                    var url = Pagination.getUrl(opts.url,SelectedItem[0].value);
                    window.location.href = url;
               })
            });

            return $select;

        },
        init:function ($obj) {
            $obj.each(function () {
                var $self = $(this),
                    page = $self.data('pagination');

                Pagination.render($self,page);
            });
        },
        render:function ($obj,opts) {
            if(opts.pages){
                $obj.show();
                Pagination.prive(opts).appendTo($obj);
                Pagination.select(opts).appendTo($obj);
                Pagination.next(opts).appendTo($obj);
            }
            else{
                $obj.hide();
            }
        }
    }
    return Pagination;
});