define(["jquery","pagination"], function($,Pagination) {
    var Page = {
        load:function () {
            var $body = $("body");
            $.get('header.html',function (html) {
                $body.after(html);
            });
            $.get("footer.html",function (html) {
                $body.append(html);
            })
        },
        gototop:function () {
            var $obj = $("<div class='app-gototop'>∧<br/>TOP</div>");
            $obj.appendTo("body");
            $obj.on('tap click',function(){
                $('html,body').animate({scrollTop: '0px'}, 500,'swing');
            });
            $(window).scroll(function (e) {
                if($("html").scrollTop()>100){
                    $obj.fadeIn(300);
                }
                else{
                    $obj.fadeOut(300);
                }
            })

        }
    }
    Page.gototop();
    Pagination.init($(".app-pagination"));
    return Page;
});