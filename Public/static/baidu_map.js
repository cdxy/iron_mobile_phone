/**
 * Created by Mr.Li<LHB2002@qq.com> on 2017-07-20.
 */

var MAP_UI = {
    icon_url:"image/icon.png",
    create:function (obj,map_data) {
        var map = this.createMap(obj,map_data);
        this.addMapControl(map);
        this.setMapEvent(map);
        this.addMarker(map,map_data);
		return map;
    },
    createMap:function (obj,json) {
        var map = new BMap.Map(obj);
        var p0 = json.point.split("|")[0];
        var p1 = json.point.split("|")[1];
        var point = new BMap.Point(p0,p1);
        map.centerAndZoom(point,14);
        return map;
    },
    /*地图控件添加函数：*/
    addMapControl:function(map){
        //向地图中添加缩放控件
        var ctrl_nav = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});
        map.addControl(ctrl_nav);
        //向地图中添加比例尺控件
        var ctrl_sca = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});
        map.addControl(ctrl_sca);
    },
    /*地图事件设置函数：*/
    setMapEvent:function(map){
        map.enableDragging();
        map.enableScrollWheelZoom();
        map.enableDoubleClickZoom();
        map.enableKeyboard();
    },
    /*创建InfoWindow*/
    createInfoWindow:function(json){
        var iw = new BMap.InfoWindow("<b class='iw_poi_title' title='" + json.title + "'>" + json.title + "</b><div class='iw_poi_content'>"+json.content+"</div>");
        return iw;
    },
    /*创建一个Icon*/
    createIcon:function (json){
        var icon = new BMap.Icon(this.icon_url, new BMap.Size(json.w,json.h),{imageOffset: new BMap.Size(-json.l,-json.t),infoWindowOffset:new BMap.Size(json.lb+5,1),offset:new BMap.Size(json.x,json.h)})
        return icon;
    },
    /*添加marker*/
    addMarker:function (map,json) {
        var p0 = json.point.split("|")[0];
        var p1 = json.point.split("|")[1];
        var point = new BMap.Point(p0,p1);
        var iconImg = this.createIcon(json.icon);
        var marker = new BMap.Marker(point,{icon:iconImg});
        var iw = this.createInfoWindow(json);
        var label = new BMap.Label(json.title,{"offset":new BMap.Size(json.icon.lb-json.icon.x+10,-20)});
        marker.setLabel(label);
        map.addOverlay(marker);
        label.setStyle({
            borderColor:"#808080",
            color:"#333",
            cursor:"pointer"
        });
        /*默认显示对话框*/
        marker.openInfoWindow(iw);
        marker.addEventListener("click",function(){
            this.openInfoWindow(iw);
        });
        iw.addEventListener("open",function(){
            marker.getLabel().hide();
        });
        iw.addEventListener("close",function(){
            marker.getLabel().show();
        });
        label.addEventListener("click",function(){
            marker.openInfoWindow(iw);
        });
    }


}
