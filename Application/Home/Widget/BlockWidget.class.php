<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Widget;
use Think\Controller;

/**
 * 区块widget
 * 用于动态调用分类信息
 */

class BlockWidget extends Controller{

    const CACHE_MENU = 'cache_menu';
    /**
     * 缓存子菜单信息
     */
    const CACHE_SUB_MENU = 'cache_sub_menu_';
    /**
     * 右侧报名信息
     * @author Mr.Li<lhb2002@qq.com>
     */
    public function left(){
        $this->display('Widget/left');
    }

    /**
     * @param $controller_name
     * @param $action_name
     * @param $query
     * @author Mr.Li<lhb2002@qq.com>
     */
    public function subNav($controller_name, $action_name, $query = array()){

        $channel_db = D('Channel');
        $map = array(
            'url'=>array()
        );
        foreach ($query as $key=>$val){
            $map['url'][] = array('EXP',"REGEXP '{$controller_name}/{$action_name}\\?.*(.*{$key}={$val})'");
        }
        trace($map['url']);
        $current_channel = $channel_db->where($map)->find();
        if($pid = $current_channel['pid']){
            $current_channel = $channel_db->where(array('id'=>$pid))->find();
            //三级菜单获取
            if($current_channel['pid']){
                $pid =  $current_channel['pid'];
            }
        }
        else{
            $pid = $current_channel['id'];
        }
        $sub_menu = $this->getSubMenu($pid);
        $this->assign('submenu',$sub_menu);
        $this->display('Widget/submenu');
    }

    /**
     * 获取子菜单
     * @param $pid
     * @author Mr.Li<lhb2002@qq.com>
     */
    private function getSubMenu($pid){
        $sub_menu = S(self::CACHE_SUB_MENU . $pid);
        if(APP_DEBUG || !$sub_menu){
            $channel_db = D('Channel');
            $sub_menu = $channel_db->find($pid);

            $menu_data = $this->getMenu();
            $sub_menu['_'] = list_to_tree($menu_data,"id", "pid", "_" ,$pid);
            S(self::CACHE_SUB_MENU . $pid,$sub_menu);
        }

        return $sub_menu;
    }

    private function getMenu(){
        $menu = S(self::CACHE_MENU);
        if(APP_DEBUG || !$menu){
            $menu = D('Channel')->where("status=1")->order("sort")->select();

            S(self::CACHE_MENU,$menu);
        }
        return $menu;
    }

    /**
     * 面包屑
     * @param $cid
     * @author Mr.Li<lhb2002@qq.com>
     */
    public function breadcrumb($cid){
        $crumbs = $this->get_bread($cid);
        echo $crumbs;
    }

    /**
     * 获取面包屑
     * 基于分类生成分类面包屑
     * @param $id 当前分类ID
     */
    private  function get_bread($id){
        //查询pid
        $category_name = get_category($id,'name');
        $pid = get_category($id,'pid');
        //根据当前分类是否允许发布内容来确定用index模版还是lists模版
        $url = get_category($id,'allow_publish')?U('Home/Article/lists',array('category'=>$category_name)):'javascrip:;';
        $link = '<a href="[url]" alt="[title]"><p>[title] <i class="icon iconfont icon-xiangyou"></i></p></a>';
        $str = $pid?
            $this->get_bread($pid):
            '';
        $str .= str_replace(
            array('[url]','[title]'),
            array($url,get_category_title($id)),
            $link
        );
        return $str;
    }

    /**
     * banner
     * @author Mr.Li<lhb2002@qq.com>
     */
    public function banner($controller_name, $action_name,$category = ''){
        $adv_id = 7;
        $categories = array(
            'about' =>  7,      //关于
        );
        if(key_exists($category,$categories)){
            $adv_id = $categories['$category'];
        }
        elseif('Index'==$controller_name && 'index'==$action_name){
            $adv_id = 6;
        }
        hook('Advs',array('tmpl'=>'banner','id'=>$adv_id));
    }

    /**
     * 左侧联系我们区块内容
     * @author Mr.Li<lhb2002@qq.com>
     */
    public function Contact(){
        $this->display('Widget/contact');
    }

    public function prduct_menu(){
        $categories = D("category")->getTree(2);
        $this->assign('categories',$categories);
        $this->display('Widget/prduct_menu');
    }
}
