<?php
/**
 * Author: Mr.Li<LHB2002@qq.com>
 * Date: 2018-07-17 10:00
 */

namespace Home\Controller;


class SearchController extends HomeController
{
    public function index(){
        $key = I('key');
        $keys = str2arr($key,' ');
        foreach ($keys as &$k){
            $k = "%{$k}%";
        }
        $model = D('Document');
        $page = I('p',1,'intval');
        $pagesize = I('ps',10,'intval');
        $map = array();

        $map['D.title'] = array('like',$keys,'and');

        list($list,$count) = $model->search($map,"{$page},{$pagesize}");

        $page = new \Think\Page($count,$pagesize,array('key'=>$key));
        $this->assign('pages',$page->show());
        $this->assign('count',$count);
        $this->assign('list',$list);
        $this->display();
    }
}