<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Controller;
use Think\Model;
use User\Api\UserApi;

/**
 * 用户控制器
 * 包括用户中心，用户登录及注册
 */
class UserController extends HomeController {

	/* 用户中心首页 */
	public function index(){
		
	}

	/* 注册页面 */
	public function register($username = '', $password = '', $repassword = '', $verify = ''){
        if(!C('USER_ALLOW_REGISTER')){
            $this->error('注册已关闭');
        }
		if(IS_POST){ //注册用户
			/* 检测验证码 */
			if(!$this->check_sms_verify(1,$username,$verify)){
				$this->error('验证码输入错误！');
			}

			/* 检测密码 */
			if($password != $repassword){
				$this->error('密码和重复密码不一致！');
			}			

			/* 调用注册接口注册用户 */
            $User = new UserApi;
			$uid = $User->register($username, $password, '',$username);
			if(0 < $uid){ //注册成功
				//TODO: 发送验证邮件
				$this->success('注册成功！',U('login'));
			} else { //注册失败，显示错误信息
				$this->error($this->showRegError($uid));
			}

		} else { //显示注册表单
			$this->display();
		}
	}

	/* 登录页面 */
	public function login($username = '', $password = '', $verify = ''){
		if(IS_POST){ //登录验证
			/* 检测验证码 */
			if(!check_verify($verify)){
				//$this->error('验证码输入错误！');
			}

			/* 调用UC登录接口登录 */
			$user = new UserApi;

            $login_type = 1;
            if(preg_match("/^1[345678]{1}\d{9}$/",$username)){
                $login_type = 3;
            }
            $uid = $user->login($username, $password,$login_type);
			if(0 < $uid){ //UC登录成功
				/* 登录用户 */
				$Member = D('Member');

				$url = Cookie('__home_forward__');
				/*首次登陆的用户跳转到认证页面*/
				$user = $Member->find($uid);
				if(empty($user)){
                    $url = U('Home/User/authentication');
                }
                /*没有记录调整页面的时候，跳转到主页*/
				else if(empty($url)){
                    $url = U('Home/Index/index');
                }
				if($Member->login($uid)){ //登录用户
					//TODO:跳转到登录前页面
					$this->success('登录成功！',$url);
				} else {
					$this->error($Member->getError());
				}

			} else { //登录失败
				switch($uid) {
					case -1: $error = '用户不存在或被禁用！'; break; //系统级别禁用
					case -2: $error = '密码错误！'; break;
					default: $error = '未知错误！'; break; // 0-接口参数错误（调试阶段使用）
				}
				$this->error($error);
			}

		} else { //显示登录表单
			$this->display();
		}
	}

	/* 退出登录 */
	public function logout(){
		if(is_login()){
			D('Member')->logout();
			$this->success('退出成功！', U('User/login'));
		} else {
			$this->redirect('User/login');
		}
	}

	/* 验证码，用于登录和注册 */
	public function verify(){
		$verify = new \Think\Verify();
		$verify->entry(1);
	}

	/**
	 * 获取用户注册错误信息
	 * @param  integer $code 错误编码
	 * @return string        错误信息
	 */
	private function showRegError($code = 0){
		switch ($code) {
			case -1:  $error = '用户名长度必须在16个字符以内！'; break;
			case -2:  $error = '用户名被禁止注册！'; break;
			case -3:  $error = '用户名被占用！'; break;
			case -4:  $error = '密码长度必须在6-30个字符之间！'; break;
			case -5:  $error = '邮箱格式不正确！'; break;
			case -6:  $error = '邮箱长度必须在1-32个字符之间！'; break;
			case -7:  $error = '邮箱被禁止注册！'; break;
			case -8:  $error = '邮箱被占用！'; break;
			case -9:  $error = '手机格式不正确！'; break;
			case -10: $error = '手机被禁止注册！'; break;
			case -11: $error = '手机号被占用！'; break;
			default:  $error = '未知错误';
		}
		return $error;
	}


    /**
     * 修改密码提交
     * @author huajie <banhuajie@163.com>
     */
    public function profile(){
		if ( !is_login() ) {
			$this->error( '您还没有登陆',U('User/login') );
		}
        if ( IS_POST ) {
            //获取参数
            $uid        =   is_login();
            $password   =   I('post.old');
            $repassword = I('post.repassword');
            $data['password'] = I('post.password');
            empty($password) && $this->error('请输入原密码');
            empty($data['password']) && $this->error('请输入新密码');
            empty($repassword) && $this->error('请输入确认密码');

            if($data['password'] !== $repassword){
                $this->error('您输入的新密码与确认密码不一致');
            }

            $Api = new UserApi();
            $res = $Api->updateInfo($uid, $password, $data);
            if($res['status']){
                $this->success('修改密码成功！');
            }else{
                $this->error($res['info']);
            }
        }else{
            $this->display();
        }
    }

    /**
     * 认证会员
     * Author: Mr.Li<LHB2002@qq.com>
     * Date: 2018-06-25 15:08
     */
    public function authentication(){
        $Member = D('Member');
        $uid        =   is_login();
        if ( IS_POST ) {
            //获取参数
            $data = I('post.');
            if($data['mobile']){
                $uc_model = M('UcenterMember');
                if($uc_model->where(array('mobile'=>$data['mobile']))->find()){
                    $this->error('手机号码已存在，不允许再次使用');
                }
                else{
                    $uc_model->where(array('id'=>$uid))->setField('mobile',$data['mobile']);
                }
            }
            $data = $Member->create($data,2);
            $status = $Member->where(array('uid'=>$uid))->save($data);
            if($status){
                $this->success('申请提交成功,等待系统审核！');
            }else{
                $this->error($Member->getError());
            }
        }else{
            $info = $Member->alias('M')
                           ->join('__UCENTER_MEMBER__ UCM on M.uid = UCM.id')
                           ->field('M.*,UCM.mobile')
                           ->where(array('M.uid'=>$uid))
                           ->find();
            $this->assign('info',$info);
            $this->display();
        }
    }

    public function code(){
        $mobile = I('post.phone');
        $conent = "【永律通】验证码为{code},有效期为10分钟";
        $code = $this->sms_verify(1,$mobile);
        $conent = str_replace('{code}',$code,$conent);

        vendor('SMS.Autoloader');
        $sms = new \lhb\SMS\Code();
        $overage = $sms->send($mobile,$conent);
        if($overage){
            $this->ajaxReturn(array(
                'status'=>1,
                'info'=>'发送成功'
            ));
        }
        else{
            $this->ajaxReturn(array(
                'status'=>0,
                'info'=>$sms->getError()
            ));
        }
    }

    private function sms_verify($type = 1, $mobile){
        $code = $this->get_verify_code();
        S("SMS_{$type}_{$mobile}",$this->verify_auth($code),60*10);
        return $code;
    }

    private function check_sms_verify($type = 1, $mobile,$code){
        $code_auth = S("SMS_{$type}_{$mobile}");
        return $code_auth == $this->verify_auth($code);
    }
    private function get_verify_code($length = 6){
        $codeSet = '0123456789';
        $code = '';
        for($i = 0 ; $i < $length ; $i++){
            $code.= $codeSet[mt_rand(0, strlen($codeSet) - 1)];
        }
        return $code;
    }

    /* 加密验证码 */
    private function verify_auth($str,$key='ylt')
    {
        return md5($key . $str);
    }


    public function download($p = 1){
        $uid = is_login();

        $data = M('UserDownload')->alias('UD')
                              ->join('__DOCUMENT__ D on D.id = UD.download_id')
                              ->where(array('UD.uid'=>$uid,'D.status'=>1))
                              ->field('D.id,D.title,UD.download_time')
                              ->page($p,10)
                              ->order('UD.download_time desc')
                              ->select();

        $this->assign('data',$data);
        $this->display();
    }
}
