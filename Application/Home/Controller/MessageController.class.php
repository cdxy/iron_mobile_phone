<?php

namespace Home\Controller;

/**
 * 前台留言控制器
 */
class MessageController extends HomeController {

    public function index(){
        $this->display('Service/message');

    }
	//
    public function add(){
        $message_db = D('Message');
        $data = $message_db->create();
        $id = $message_db->add();
        if($id){
            $this->sendMail($data);
            $this->success('添加成功');
        }
        else{
            $this->error('添加失败'.$message_db->getError());
        }
    }
    private function getMailBody($data){
        $str = '';
        $str.="姓名：{$data['name']}<br/>";
        $str.="手机号码：{$data['phone']}<br/>";
        $str.="邮箱：{$data['email']}<br/>";
        $str.="咨询内容：{$data['content']}<br/>";

        return $str;
    }
    private function sendMail($data){
        $subject = '官网留言信息';
        $body = $this->getMailBody($data);

        $secure = C('SMTP_SECURE');
        $host =  C('SMTP_HOST');
        $port = C('SMTP_PORT');
        $username =  C('SMTP_USER');
        $password = C('SMTP_PASS');
        $from_email = C('FROM_EMAIL');
        $from_name =  C('FROM_NAME');
        $to_mail = C('TO_EMAIL');

        if(empty($host) || empty($port) || empty($username) || empty($password) || empty($from_email) || empty($from_name)){
            \Think\Log::write('邮件发送服务器配置错误' ,'Mail state');
            return false;
        }

        vendor('PHPMailer.PHPMailerAutoload'); //从PHPMailer目录导class.phpmailer.php类文件
        $mail             = new \PHPMailer(); //PHPMailer对象
        $mail->CharSet    = 'UTF-8'; //设定邮件编码，默认ISO-8859-1，如果发中文此项必须设置，否则乱码
        $mail->IsSMTP();  // 设定使用SMTP服务
        $mail->SMTPDebug  = 0;                     // 关闭SMTP调试功能
        // 1 = errors and messages
        // 2 = messages only
        $mail->SMTPAuth   = true;                  // 启用 SMTP 验证功能
        $mail->SMTPSecure = $secure;                 // 使用安全协议
        $mail->Host       = $host;  // SMTP 服务器
        $mail->Port       = $port;  // SMTP服务器的端口号
        $mail->Username   = $username;  // SMTP服务器用户名
        $mail->Password   = $password;  // SMTP服务器密码
        $mail->SetFrom($from_email,$from_name);

        $mail->Subject    = $subject;
        $mail->MsgHTML($body);

        //添加收件人信息
        $mail->AddAddress(C('TO_EMAIL'), C('TO_EMAIL'));

        $mailstate= $mail->Send();
        if($mailstate){
            \Think\Log::write('邮件发送成功' ,'Mail state');
            return true;
        }
        else{
            $errormes=$mail->ErrorInfo;
            \Think\Log::write('邮件发送失败：' . $errormes,'Mail state');
            return  $errormes;
        }
    }


}