<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Controller;
use Think\Controller;

/**
 * 前台公共控制器
 * 为防止多分组Controller名称冲突，公共Controller名称统一使用分组名称
 */
class HomeController extends Controller {

    /**
     * 页面基础参数
     * Author: Mr.Li<LHB2002@qq.com>
     * Date: 2018-07-17 16:55
     */
    protected function setConfig(){

        $modulename = MODULE_NAME;
        $controllername = CONTROLLER_NAME;
        $actionname = ACTION_NAME;


        $config = [
            'modulename'     => $modulename,
            'controllername' => $controllername,
            'actionname'     => $actionname,
        ];

        $this->assignConfig($config);
    }

    /**
     * 改写配置值
     * @param $name
     * @param string $value
     * Author: Mr.Li<LHB2002@qq.com>
     * Date: 2018-07-17 16:55
     */
    protected function assignConfig($name, $value=''){
        $this->merge('config',$name, $value);
    }

    /**
     * 合并view的赋值参数
     * @param $name
     * @param $key
     * @param string $value
     * @throws AppException
     * Author: Mr.Li<LHB2002@qq.com>
     * Date: 2018-07-17 17:09
     */
    protected function merge($name, $key, $value=''){
        $data = $this->view->get($name);
        if(is_null($data) || false===$data){
            $data = [];
        }
        if(!is_array($data)){
            throw new \Exception('原始变量不是数组');
        }
        $data = array_merge($data,is_array($key)?$key:[$key=>$value]);
        $this->view->assign($name,$data);
    }

	/* 空操作，用于输出404页面 */
	public function _empty(){
		$this->redirect('Index/index');
	}


    protected function _initialize(){
        /* 读取站点配置 */
        $config = api('Config/lists');
        C($config); //添加配置

        if(!C('WEB_SITE_CLOSE')){
            $this->error(L('web_site_close'));
        }

        if(defined('LANG_THEME') ){
            $this->theme(LANG_THEME);
        }

        $this->setConfig();
        $this->WechatConfig();
    }

    protected function WechatConfig(){
	    //if(is_wechat()){
            $wechat_api = new \Common\Api\WechatApi();
            //$wechat_api->OAuth();
            $this->assignConfig('wechat_jssign',$wechat_api->getJsSign());
            $this->assignConfig('is_wechat',true);

       // }
    }

	/* 用户登录检测 */
	protected function login(){
		/* 用户登录检测 */
		is_login() || $this->error(L('un_login'), U('User/login'));
	}

}
