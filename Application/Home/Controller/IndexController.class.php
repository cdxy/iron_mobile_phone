<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Controller;
use OT\DataDictionary;

/**
 * 前台首页控制器
 * 主要获取首页聚合数据
 */
class IndexController extends HomeController {

	//系统首页
    public function index(){
        //获取首页指定的视频信息
        $this->display();
    }

    /**
     * 异步获取文档信息
     * @author Mr.Li<lhb2002@qq.com>
     */
    public function ajax(){
        $data = array(
            'category'=>array(),
            'data'=>array(),
            'status'=>1
        );
        $category_id = I('get.category');
        $size = I('get.size');
        $order = I('get.order','');
        $model = I('get.model','Article');
        $data['id'] = I('get.obj');
        //获取当前分类信息
        $data['category']  = $category= get_category($category_id);
        if(empty( $data['category'])){
            $data['status'] = 0;
            $this->ajaxReturn($data);
        }

        //转换链接信息
        $data['category']['url'] = U('Article/lists',array('category'=>$category['name']));
        $data['category']['iconurl'] = get_cover($category['icon'],'path');

        $join = array(
            'left join __PICTURE__ P on P.id = D.cover_id',
            'left join __DOCUMENT_'.strtoupper($model) .'__ A on A.id = D.id'
        );
        $map = array(
            'category_id' =>     $category_id,
            'D.status'=>1
        );
        $field = array(
            'D.id',
            'D.title',
            'D.description',
            'D.create_time',
            'D.view',
            'P.path'=>'cover_url',
        );
        $articles = D('Document')->alias('D')
                                  ->join($join)
                                  ->limit($size)
                                  ->order($order)
                                  ->where($map)
                                  ->field($field)
                                  ->select();

        //转换链接信息
        $data['data'] = array_map(function ($val) use ($category) {
            $val['url'] = U('Article/detail',array('id'=>$val['id'],'category'=>$category['name']));
            return $val;
        },$articles);

        $this->ajaxReturn($data);
    }

}