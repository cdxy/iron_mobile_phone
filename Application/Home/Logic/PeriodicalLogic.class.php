<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Logic;

use Think\Exception;
/**
 * 文档模型子模型 - 期刊模型
 */
class PeriodicalLogic extends BaseLogic{

    /**
     * 获取模型详细信息
     * @param  integer $id 文档ID
     * @return array       当前模型详细信息
     */
    public function detail($id){
        $data = $this->field(true)->find($id);
        if(!$data){
            $this->error = '获取详细信息出错！';
            return false;
        }
        $FileModel = D('File');
        $root = substr(C('DOWNLOAD_UPLOAD.rootPath'), 1);
        $pdf = $FileModel->find($data['pdf']);
        $filePath = $root.$pdf['savepath'].$pdf['savename'];
        $data['filePath'] = $filePath;
        return $data;
    }

	/**
	 * 下载文件
	 * @param  number $id 文档ID
	 * @return boolean    下载失败返回false
	 */
	public function download($id,$grade=0){
		$info = $this->find($id);
		if(empty($info)){
			$this->error = "不存在的文档ID：{$id}";
			return false;
		}

		if( $grade < $info['grade'] ){
            throw new Exception("你没有权限下载",-999);
        }

		$File = D('File');
		$root = C('DOWNLOAD_UPLOAD.rootPath');
		$call = array($this, 'setDownload');
		if(false === $File->download($root, $info['file_id'], $call, $info['id'])){
			$this->error = $File->getError();
		}
	}


}
