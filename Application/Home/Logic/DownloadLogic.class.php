<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Logic;

use Think\Exception;
/**
 * 文档模型子模型 - 下载模型
 */
class DownloadLogic extends BaseLogic{

	/* 自动验证规则 */
	protected $_validate = array(
		array('content', 'require', '内容不能为空！', self::MUST_VALIDATE , 'regex', self::MODEL_BOTH),
	);

	/* 自动完成规则 */
	protected $_auto = array();

	public function update($id){
		/* 获取下载数据 */ //TODO: 根据不同用户获取允许更改或添加的字段
		$data = $this->field('download', true)->create();
		if(!$data){
			return false;
		}

		$file = json_decode(think_decrypt(I('post.file')), true);
		if(!empty($file)){
			$data['file_id'] = $file['id'];
			$data['size']    = $file['size'];
		} else {
			$this->error = '获取上传文件信息失败！';
			return false;
		}
		
		/* 添加或更新数据 */
		if(empty($data['id'])){//新增数据
			$data['id'] = $id;
			$id = $this->add($data);
			if(!$id){
				$this->error = '新增详细内容失败！';
				return false;
			}
		} else { //更新数据
			$status = $this->save($data);
			if(false === $status){
				$this->error = '更新详细内容失败！';
				return false;
			}
		}

		return true;
	}

	/**
	 * 下载文件
	 * @param  number $id 文档ID
	 * @return boolean    下载失败返回false
	 */
	public function download($id,$grade=0){
		$info = $this->find($id);
		if(empty($info)){
			$this->error = "不存在的文档ID：{$id}";
			return false;
		}
        $user_download_count = 0;
        $uid = is_login();
        if(1==$grade && $info['anonymous'] && $uid){
		    //允许下载，判断是否超出下载次数
            $user_download_count = D('Member')->where(array('uid'=>$uid))->getField('download_count');
            if(1>$user_download_count){
                throw new Exception("你已没有免费下载次数",-999);
            }
        }
		else if( $grade < $info['grade'] ){
            throw new Exception("你没有权限下载",-999);
        }

		$File = D('File');
		$root = C('DOWNLOAD_UPLOAD.rootPath');
        $self = $this;
		if(false === $File->download(
		    $root,
            $info['file_id'],
            function ($id) use ($self,$user_download_count,$uid){
		        /*下载回调方法*/
                //更新下载次数
                $self->where(array('id' => $id))->setInc('download');
                //更新用户可用下载次数
                D('Member')->where(array('uid'=>$uid))->setDec('download_count');

                //记录用户下载记录
                if($uid){
                    M('UserDownload')->add(array(
                        'uid'=>$uid,
                        'download_id'=>$id,
                        'download_time' => NOW_TIME
                    ));
                }
            } ,
            $info['id'])
        ){
			$this->error = $File->getError();
		}
	}

	/**
	 * 新增下载次数（File模型回调方法）
	 */
	public function setDownload($id){
		$map = array('id' => $id);
		$this->where($map)->setInc('download');
	}

}
