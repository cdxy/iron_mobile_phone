<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

/**
 * 前台公共库文件
 * 主要定义前台公共函数库
 */

/**
 * 检测验证码
 * @param  integer $id 验证码ID
 * @return boolean     检测结果
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
function check_verify($code, $id = 1){
	$verify = new \Think\Verify();
	return $verify->check($code, $id);
}

/**
 * 获取列表总行数
 * @param  string  $category 分类ID
 * @param  integer $status   数据状态
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
function get_list_count($category, $status = 1){
    static $count;
    if(!isset($count[$category])){
        $count[$category] = D('Document')->listCount($category, $status);
    }
    return $count[$category];
}

/**
 * 获取段落总数
 * @param  string $id 文档ID
 * @return integer    段落总数
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
function get_part_count($id){
    static $count;
    if(!isset($count[$id])){
        $count[$id] = D('Document')->partCount($id);
    }
    return $count[$id];
}

/**
 * 获取导航URL
 * @param  string $url 导航URL
 * @return string      解析或的url
 * @author 麦当苗儿 <zuojiazi@vip.qq.com>
 */
function get_nav_url($url){
    switch ($url) {
        case 'http://' === substr($url, 0, 7):
        case '#' === substr($url, 0, 1):
            break;        
        default:
            $url = U($url);
            break;
    }
    return $url;
}

/**
 * 获取文件扩展名
 * @param $fileurl
 * @author Mr.Li<lhb2002@qq.com>
 */
function getExt($fileurl){
    $urlinfo =  parse_url($fileurl);
    $file = basename($urlinfo['path']);
    if(strpos($file,'.') !== false)
    {
        $ext = explode('.',$file);
        return $ext[count($ext)-1];
    }
    return false;
}

/**
 * 获取本地名称名称
 * @param $name
 * @param $mode
 * @param $case 转换模式
 *              '':不干预
 *              upper:大写
 *              lower:小写
 * @author Mr.Li<lhb2002@qq.com>
 */
function getLoaclName($name,$min = true,$case = ''){
    if( is_string($name) && defined('LANG_SET') )
    {
        $lang_set = LANG_SET;
        $default_lang  = C('DEFAULT_LANG');
        if( 'upper' === $case ){
            $lang_set = strtoupper($lang_set);
            $name = strtoupper($name);
            $default_lang = strtoupper($default_lang);
        }
        elseif ( 'lower' === $case ){
            $lang_set = strtolower($lang_set);
            $name = strtolower($name);
            $default_lang = strtolower($default_lang);
        }

        if ( $lang_set != $default_lang ) {
            if( $min && stripos($name, $lang_set) === 0 ){
                return substr($name, strlen($lang_set . '_'));
            }
            elseif (!$min && 0 !== stripos($name, $lang_set) ){
                return $lang_set . '_' . $name;
            }
        }
    }

    return $name;
}

/**
 * 获取下载分类树信息
 * Author: Mr.Li<LHB2002@qq.com>
 * Date: 2018-06-25 10:31
 */
function get_category_tree($pid = 'download'){
    $key = "CATEGORY_TREE_{$pid}";
    $data = S($key);
    if(empty($data)){
        $data = D('Category')->getTree($pid);
        S($key,$data,60);
    }

    return $data;
}