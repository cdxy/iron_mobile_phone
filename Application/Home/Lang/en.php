<?php
/**
 * Created by PhpStorm.
 * User: Mr.Li<LHB2002@qq.com>
 * Date: 2017-07-28
 * Time: 15:38
 */

return array(
    'home_title'            =>      'Home',
    'web_site_close'        =>      'The site is closed. Please visit later~',
    'un_login'               =>     'You are not logged in yet. Please login first!',
    /*分页*/
    'page_prev'             =>      'Prev',
    'page_next'             =>      'Next',
    'page_first'            =>      'First',
    'page_last'             =>      'Last',

    /*article控制器*/
    'failed_data'           =>      'Failed to get list data',
    'article_id_error'     =>       'Article ID Error!',
    'category_is_empty'    =>       'Category is empty!',
    'category_display'     =>       'Category disable display!',
    'category_exist'       =>       'Category does not exist or is disabled!',

    /*留言页面*/
    'message_title'       =>       'Title',
    'message_content'     =>       'Content',
    'message_nickname'    =>       'Nickname',
    'message_phone'        =>       'Phone',
    'message_address'      =>       'Address',
    'message_email'        =>       'E-Mail',
    'message_verify'       =>       'Verify',

    'message_title_placeholder'       =>       'Please enter title',
    'message_content_placeholder'     =>       'Please enter content',
    'message_nickname_placeholder'    =>       'Please enter nickname',
    'message_phone_placeholder'        =>       'Please enter phone',
    'message_address_placeholder'      =>       'Please enter address',
    'message_email_placeholder'        =>       'Please enter E-mail',
    'message_qq_placeholder'           =>       'Please enter QQ',

    'message_btn_submit'  =>        'Submit',
    'message_btn_cancel'  =>        'Cancel',
);