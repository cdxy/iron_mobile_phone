<?php
/**
 * Created by PhpStorm.
 * User: Mr.Li<LHB2002@qq.com>
 * Date: 2017-07-28
 * Time: 15:38
 */

return array(
    'home_title'            =>      '主页',
    'web_site_close'        =>      '站点已经关闭，请稍后访问~',
    'un_login'               =>     '您还没有登录，请先登录！',

    /*分页*/
    'page_prev'             =>      '上一页',
    'page_next'             =>      '下一页',
    'page_first'            =>      '首页',
    'page_last'             =>      '尾页',

    /*article控制器*/
    'failed_data'           =>      '获取列表数据失败',
    'article_id_error'     =>       '文档ID错误！',
    'category_is_empty'    =>       '没有指定文档分类!',
    'category_display'     =>       '该分类禁止显示！',
    'category_exist'       =>       '分类不存在或被禁用！',

    /*留言页面*/
    'message_title'       =>       '标题',
    'message_content'     =>       '内容',
    'message_nickname'    =>       '您的称呼',
    'message_phone'        =>       '联系电话',
    'message_address'      =>       '联系地址',
    'message_email'        =>       '电子邮件',
    'message_verify'       =>       '验证码',

    'message_title_placeholder'       =>       '请输入留言标题',
    'message_content_placeholder'     =>       '留言内容',
    'message_nickname_placeholder'    =>       '请输入真实姓名',
    'message_phone_placeholder'        =>       '请输入联系电话',
    'message_address_placeholder'      =>       '请输入联系地址',
    'message_email_placeholder'        =>       '请输入电子邮件',
    'message_qq_placeholder'           =>       '请输入QQ',


    'message_btn_submit'  =>        '提交',
    'message_btn_cancel'  =>        '取消',


);