<?php
/**
 * Created by PhpStorm.
 * User: Mr.Li<LHB2002@qq.com>
 * Date: 2017-09-12
 * Time: 15:40
 */

//路由配置信息
return array(
    //开启路由模式
    'URL_ROUTER_ON'   => true,
    //静态路由
    'URL_MAP_RULES'=>array(
        'admin'=>'Admin/Index/index',
        'index'=>'Index/index',
        'about/introduction'=>'Article/detail?id=1&category=about',
        'about/culture'=>'Article/detail?id=2&category=about',


        'contact/us'=>'Article/detail?id=57&category=contact',

        //列表页
        'about/honor'=>'Article/lists?category=honor',

        'products/welding'  =>  'Article/lists?category=welding',
        'products/laser_cutting'  =>  'Article/lists?category=laser_cutting',
        'products/yoke_punching'  =>  'Article/lists?category=yoke_punching',

        'metal'=>'Article/lists?category=metal',
        'equipment' =>  'Article/lists?category=equipment',
        'guide' =>  'Article/lists?category=guide',

        'news/company' =>  'Article/lists?category=company_news',
        'news/industry' =>  'Article/lists?category=industry_news',

    ),
    //规则路由
    'URL_ROUTE_RULES'=>array(
        //%5BPAGE%5D|\d*  %5BPAGE%5D：分页插件中的[PAGE]
        //列表分页
        '/^about\/honor_(%5BPAGE%5D|\d*)$/'  =>  'Article/lists?category=honor&p=:1',

        '/^products\/welding_(%5BPAGE%5D|\d*)$/'  =>  'Article/lists?category=welding&p=:1',
        '/^products\/laser_cutting_(%5BPAGE%5D|\d*)$/'  =>  'Article/lists?category=laser_cutting&p=:1',
        '/^products\/yoke_punching_(%5BPAGE%5D|\d*)$/'  =>  'Article/lists?category=yoke_punching&p=:1',

        '/^metal_(%5BPAGE%5D|\d*)$/'  =>  'Article/lists?category=metal&p=:1',
        '/^equipment_(%5BPAGE%5D|\d*)$/'  =>  'Article/lists?category=equipment&p=:1',
        '/^guide_(%5BPAGE%5D|\d*)$/'  =>  'Article/lists?category=guide&p=:1',

        '/^news\/company_(%5BPAGE%5D|\d*)$/'  =>  'Article/lists?category=company_news&p=:1',
        '/^news\/industry_(%5BPAGE%5D|\d*)$/'  =>  'Article/lists?category=industry_news&p=:1',

        //详情内容
        '/^products\/welding\/(\d+)$/'  =>  'Article/detail?category=welding&id=:1',
        '/^products\/laser_cutting\/(\d+)$/'  =>  'Article/detail?category=laser_cutting&id=:1',
        '/^products\/yoke_punching\/(\d+)$/'  =>  'Article/detail?category=yoke_punching&id=:1',

        '/^metal\/(\d+)$/'  =>  'Article/detail?category=metal&id=:1',
        '/^equipment\/(\d+)$/'  =>  'Article/detail?category=equipment&id=:1',
        '/^guide\/(\d+)$/'  =>  'Article/detail?category=guide&id=:1',

        '/^news\/company\/(\d+)$/'  =>  'Article/detail?category=company_news&id=:1',
        '/^news\/industry\/(\d+)$/'  =>  'Article/detail?category=industry_news&id=:1',


    )

);