<?php
namespace Common\Api;
/**
 * 微信接口
 * Class WechatApi
 * @package Common\Api
 */

class WechatApi {
    private $wechat_config = null;
    private $error='';
    public function __construct(){
        vendor('Wechat/Wechat');
        vendor('Wechat/TPWechat');
        vendor('Wechat/ErrCode');
        $this->wechat_config = load_config(COMMON_PATH.'Conf/wechat.php');
    }

    /**
     * 获取最近的错误信息
     * @return string
     */
    public function getError(){
        return $this->error;
    }

    /**
     * 获取指定页面微信js Sign信息
     * @param string $url
     * @return array|bool
     */
    public function getJsSign($url=''){
        //当前页面完整网址
        if(empty($url)){
            $url = getSelfUrl(true);
        }

        $tpwechat=new \TPWechat($this->wechat_config);
        $signPackage=$tpwechat->getJsSign($url);
        if(!$signPackage){
            dump($tpwechat->errMsg);
            $this->error = $tpwechat->errMsg;

        }
        return $signPackage;
    }




    /**
     * 获取微信用户授权登陆信息
     */
    public function OAuth($url=''){
        //静默登录
        $scope = 'snsapi_base';
        $code = isset($_GET['code'])?$_GET['code']:'';

        if(I('get.debug',false)){
            session('sns_userinfo',null);
        }

        $sns_userinfo=session('sns_userinfo');
        //判断是否获取到了sns的用户信息
        if(!$sns_userinfo ){

            //获取微信配置
            $wechat_config = $this->wechat_config;
            $wechat_config['debug']=true;
            $we_obj=new \TPWechat($wechat_config);
            if ($code) {
                $oauth_access_token = $we_obj->getOauthAccessToken();
                if (!$oauth_access_token) {
                    //重新获取授权登录
                    $get_map = I('get.');
                    unset($get_map['code']);
                    unset($get_map['state']);
                    $wx_redirect_url = U('',$get_map);
                    header('Location: ' . $wx_redirect_url);
                    exit;
                }

                //是静默方式获取登录信息
                if (strstr($oauth_access_token['scope'],'snsapi_base')!==false) {
                    $userinfo = $we_obj->getUserInfo($oauth_access_token["openid"]);
                    //静默并使用api拉取用户信息,如果为关注，拉去不到nickname信息,切换为 授权登陆模式
                    if ($userinfo && $userinfo['subscribe']==1) {
                        $userinfo['follow'] = 1 ;  //标记用户已关注平台
                    }
                    else{
                        $userinfo = false;  //不让后面的数据进行保存
                        //切换为授权登陆模式
                        $scope = 'snsapi_userinfo';
                    }
                }
                elseif (strstr($oauth_access_token['scope'],'snsapi_userinfo')!==false) {
                    //未关注，通过授权模式获取用户信息
                    $access_token = $oauth_access_token['access_token'];
                    $userinfo = $we_obj->getOauthUserinfo($access_token,$oauth_access_token["openid"]);
                    $userinfo['follow'] = 0;   //记录为未关注
                }

                if($userinfo){
                    //记录推荐人为其父用户
                    $referer=cookie('referer');
                    $userinfo['referer'] = $referer;
                    //保存用户信息
                    D('Admin/WechatUser')->update($userinfo);
                    //缓存用户信息
                    session('sns_userinfo',$userinfo);
                    //将获取到的用户信息返回到页面
                    return $userinfo;
                }


            }
            //静默方式未拉去到用户信息，或则首次获取时，执行下面代码
            if ($scope=='snsapi_base') {
                $url = $url ? $url : getSelfUrl();
                //记录当前地址，便于为关注的用户，获取授权用户信息
                session('wx_redirect',$url);
            } else {
                $url = session('wx_redirect');
            }

            //获取微信授权登陆地址
            $oauth_url = $we_obj->getOauthRedirect($url,"wxbase",$scope);
            header('Location: ' . $oauth_url);
            exit;

        }
        else{
            return $sns_userinfo;
        }
    }

    /**
     * 获取指定用户的信息
     * @param $scope
     * @param $openid
     * @param $access_token
     */
    public function getUserinfo($scope, $openid, $access_token){
        $we_obj = new \TPWechat($this->wechat_config);
        if (strstr($scope,'snsapi_base')!==false) {
            $userinfo = $we_obj->getUserInfo($openid);
            //静默并使用api拉取用户信息,如果为关注，拉去不到nickname信息,切换为 授权登陆模式
            if ($userinfo && $userinfo['subscribe']==1) {
                $userinfo['follow'] = 1 ;  //标记用户已关注平台

            }
            else{
                $userinfo = false;  //不让后面的数据进行保存
                //切换为授权登陆模式
                $scope = 'snsapi_userinfo';
            }
        }
        elseif (strstr($scope,'snsapi_userinfo')!==false) {
            //未关注，通过授权模式获取用户信息
            $access_token = $access_token;
            $userinfo = $we_obj->getOauthUserinfo($access_token,$openid);
            $userinfo['follow'] = 0;   //记录为未关注
        }
        return $userinfo;

    }


}