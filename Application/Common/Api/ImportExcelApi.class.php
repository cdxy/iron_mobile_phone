<?php
/**
 * Created by PhpStorm.
 * User: ZQ<821795078@qq.com>
 * Date: 2017/4/21
 * Time: 13:42
 */

namespace Common\Api;


class ImportExcelApi{
    //字段数据
    private $field_data = array();
    private $field_data_name = array();
    //表数量
    private $sheet;
    //行
    private $row;
    //列
    private $col;
    //回调函数
    private $callback;
    //扩展参数
    private $opts = array(
        'field'          =>     1,           //字段名所在行
        'frist_data'    =>     2,           //数据开始行
        'max_row'       =>      10000,      //单次允许导入行
        'error_write' =>       true,       //错误是否写入excel，true写入,false不写入
        'extend_data'  =>      array(),    //扩展数据
    );
    private $objPHPExcel;
    //获取类型
    private $inputFileType;
    //上传文件名
    private $file;
    //上传文件名
    private $filename;
    //错误信息
    private $mesage = array(
        'error'   =>    array(),
        'row'     =>    array(),
        'success' => 0, //正确数据条数
        'error_count'=>0,//错误数据条数
    );


    /**
     * 构造方法
     * ImportExcelModel constructor.
     */
    public function __construct()
    {
        vendor("phpExcel.PHPExcel");
    }


    /**
     * 数据导入
     * @param $file 文件路径
     * @param $field  字段
     * @param $callback  回调函数
     * @param $opts 扩展参数
     * @author ZQ<821795078@qq.com>
     */
    public static function import($file, $field, $callback,$fielname,$opts=array()){
        $obj = new self;
        $obj->_import($file,$field,$callback,$fielname,$opts);
    }

    /**
     * 数据导入处理
     * @param $file 文件路径
     * @param $field  字段
     * @param $callback  回调函数
     * @author ZQ<821795078@qq.com>
     */
    public function _import($file, $field, $callback,$filename,$opts=array()){
        $this->opts = array_merge( $this->opts ,$opts);
        $this->file = $file;
        $this->filename = $filename;
        $this->callback = $callback;
        $this->_loadFile($file);

        $this->getRowsCols();
        if( $this->row > $this->opts['max_row'] ){
            $this->mesage['error'][] = '数据已超出最大允许行数';
            //$this->removeFile();
            return false;
        }

        $this->_getField($field);
        if( empty($this->field_data) ){
            $this->mesage['error'][] = '没有获取到数据头信息';
            //$this->removeFile();
            return false;
        }

        $data = $this->_excel_data();

        if(!$data && $this->opts['error_write']){
            $this->save_error();
            $this->mesage['error_file'] = ltrim($file,'.');
        }
        else{
            //$this->removeFile();
        }

        return $data;
    }

    /**
     * 删除当前文件
     * @author Mr.Li<lhb2002@qq.com>
     */
    public function removeFile(){
        //删除文件
        unlink($this->file) || exec("rm -rf ".$this->file);
    }

    /**
     * 加载excel文件
     * @param $file 文件路径
     * @return bool
     * @author ZQ<821795078@qq.com>
     */
    public function _loadFile($file){
        if(!file_exists($file)){
            $this->mesage['error'][] =  'Execl Not Exist';
            return false;
        }
        //使用PHPExcel_IOFactory对象，注意，不能少了\   读取Excel文件

        $this->inputFileType = \PHPExcel_IOFactory::identify($file);
        $excelReader = \PHPExcel_IOFactory::createReader($this->inputFileType);
        $excelReader->setReadDataOnly(true);

        //如果有指定行数，则设置过滤器
        $startRow = 7 ;
        $endRow = 1100;
        if ($startRow && $endRow) {
            $perf           = new \PHPExcel_Reader_DefaultReadFilter();
            $perf->startRow = $startRow;
            $perf->endRow   = $endRow;
            $excelReader->setReadFilter($perf);
        }

        $this->objPHPExcel    = $excelReader->load($file);
        $this->sheet = $this->objPHPExcel->getActiveSheet();

    }

    /**
     * 获取excel表格中字段名称与数据库字段名称
     * @author ZQ<821795078@qq.com>
     */
    public function _getField($field){
        $highestColumnNum = $this->col;

        $start_position = $this->opts['field'];

        $table_head_field = array();

        for($i=0; $i<$highestColumnNum;$i++){

            $cellName = \PHPExcel_Cell::stringFromColumnIndex($i)."$start_position";
            $cellVal = $this->sheet->getCell($cellName)->getValue();
            if( $cellVal ){
                $table_head_field [$i]= $cellVal;
            }

        }

        //$this->field_data = $field;
        $this->field_data = $table_head_field;
        $this->field_data_name =  $this->compareArr($table_head_field,$field);
    }

    /**
     * 获取表格最大列，最大行值
     * @author ZQ<821795078@qq.com>
     */
    public function getRowsCols(){
        $this->row = $this->sheet->getHighestRow();
        $highestColumn  = $this->sheet->getHighestColumn();
        $this->col = \PHPExcel_Cell::columnIndexFromString($highestColumn);
    }

    /**
     * 获取excel数据
     * @param $this->opts 扩展参数
     * @author ZQ<821795078@qq.com>
     */
    public function _excel_data(){
        $result = array();
        for($row=$this->opts['frist_data']; $row<= $this->row; $row++){
            $row_data = array();
            //todo 当表头为AA后的值是看是否正常通过
            for( $col = 0; $col < $this->col; $col++ ){
                $cell = $this->sheet->getCellByColumnAndRow($col,$row);
                $value = $cell->getValue(); // ->getCalculateValue();公式计算结果
                if(is_object($value)){
                    $value = $value->__toString();
                }
                //判断是否为时间格式
                if( $cell->getDataType() == \PHPExcel_Cell_DataType::TYPE_NUMERIC ){
                    $cellstyleformat = $cell->getStyle( $cell->getCoordinate() )->getNumberFormat();
                    $formatcode = $cellstyleformat->getFormatCode();
                    if (preg_match('/^(\[\$[A-Z]*-[0-9A-F]*\])*[hmsdy]/i', $formatcode)) {
                        $value=date("Y-m-d", \PHPExcel_Shared_Date::ExcelToPHP($value));
                    }else{
                        $value = \PHPExcel_Style_NumberFormat::toFormattedString($value,$formatcode);
                    }
                }

                $row_data[$col] = $value;
            }
            //计算交集，排除掉不使用的字段列
            $row_data = array_intersect_key($row_data,$this->field_data);

            //组合字段名称
            $row_data = array_combine($this->field_data_name,$row_data);
            //如果是空数据
            if(trim(implode($row_data, '')) == '') {
                continue;
            }
            //判断是否有扩展数据
            if(!empty($this->opts['extend_data']) && is_array($this->opts['extend_data'])){
                $row_data =array_merge($this->opts['extend_data'],$row_data);
            }

            $error = '';
            $row_data = call_user_func_array($this->callback,array($row_data,&$error,$row));
            //判断有错误信息时，抛出错误信息
            if( false === $row_data || is_null($row_data) ){
                $this->mesage['error_count'] += 1;
                if($this->opts['error_write']){

                    //这里写入的错误要是数组，就不能写入表里面，必须是字符串的形式
                    $error = is_array($error) ? trim(implode(',',$error),',') : $error;

                    //排除掉没有返回值的错误，以免误导
                    $error = $error ?  $error : "未知错误";
                    $this->sheet->getCellByColumnAndRow(($col + 1 ),$row)->setValue($error);
                }
                else{
                    $this->mesage['row'][$row] = $error;
                }

            }
            else{   //只记录正确的数据
                $result[] = $row_data;
                $this->mesage['success'] += 1;
                if($this->opts['count_callback']){
                    call_user_func($this->opts['count_callback'],$row_data);
                }
            }

        }
        if( $this->mesage['error_count'] > 0 ){

            unset($result);
            return false;
        }
        else{
            return $result;
        }
    }

    /**
     * 将错误写入文件
     * @author Mr.Li<lhb2002@qq.com>
     */
    private function save_error(){
        $objphpExcel = \PHPExcel_IOFactory::createWriter($this->objPHPExcel,"$this->inputFileType");
        $objphpExcel ->save($this->file);
    }

    /**
     * 获取错误消息
     * @return string
     * @author Mr.Li<lhb2002@qq.com>
     */
    public function getError(){
        return $this->mesage;
    }

    /**
     * 判断两个数组顺序
     * @param $table_head_field 必须是索引数组
     * @param $filed 必须是关系数组
     * @return mix
     */
    function compareArr($table_head_field, $filed){
        if(!is_array($table_head_field) || !is_array($filed)){
            $this->mesage['error'][] = '字段不是有效数组';
            return false;
        }

        $filed = array_flip($filed);

        $ret_field = array();

        foreach( $table_head_field as $index=> $val ){
            if(isset($filed[$val])){
                $ret_field[] = $filed[$val];
            }
        }
        return $ret_field;
    }


    /**
     * 数据导出
     * @param $xlsName 导出文件名
     * @param $xlsCell 表头字段
     * @param $field  字段名
     * @author ZQ<821795078@qq.com>
     */
    public static function export($xlsName,$xlsCell,$xlsData,$row_callback=null){
        $obj = new self;
        $obj->exportExcel($xlsName,$xlsCell,$xlsData,$row_callback);
    }


    /**
     * 导出数据处理
     * @param $expTitle   文件名称
     * @param $expCellName  表头字段
     * @param $expTableData   数据
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @author ZQ<821795078@qq.com>
     */
    public function exportExcel($expTitle, $expCellName, $expTableData,$row_callback=null){

        $xlsTitle = iconv('utf-8', 'gb2312', $expTitle);//文件名称
        $fileName = $_SESSION['account'].date('_YmdHis');//or $xlsTitle 文件名称可根据自己情况设定

        if($expCellName===''){
            $expCellName = (unset)$expCellName;
        }

        $cellNum = count($expCellName);
        $dataNum = count($expTableData);

        $objPHPExcel = new \PHPExcel();
        $cellName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');

        $row = 1;
        //TODO 这里不知道不传表头字段信息的时候，需要合并表头信息占的单元格不，需要在处理
        if($cellNum>0){
            $objPHPExcel->getActiveSheet(0)->mergeCells('A1:'.$cellName[$cellNum-1].($row));//合并单元格
        }

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $expTitle.'  Export time:'.date('Y-m-d H:i:s'));
        //当表头信息不存在的时候，不能循环表头信息，只循环数据，存在则循环表头信息
        if($cellNum>0){
            $i = 0;
            $row++;
            foreach ($expCellName as $key=>$value){
                $objPHPExcel->setActiveSheetIndex(0)
                                    ->setCellValue($cellName[$i++].($row), $value);
            }
        }

        foreach($expTableData as $rowdata){
            $i = 0;
            $row++;
            if($row_callback){
                $rowdata = call_user_func($row_callback,$rowdata);
            }
            foreach ($expCellName as $key=>$value){
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($cellName[$i++].($row), $rowdata[$key]);
            }
        }


        // Miscellaneous glyphs, UTF-8

        ob_clean();

        header('pragma:public');
        header('Content-type:application/vnd.ms-excel;charset=utf-8;name="'.$xlsTitle.'.xls"');
        header("Content-Disposition:attachment;filename=$fileName.xls");//attachment新窗口打印inline本窗口打印
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    function __destruct()
    {
        unset($this->sheet);
        unset($this->objPHPExcel);
        unset($this->field_data);
        unset($this->mesage);
    }


}