<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: huajie <banhuajie@163.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Admin\Model\AuthGroupModel;
use Think\Page;

/**
 * 后台内容控制器
 * @author huajie <banhuajie@163.com>
 */
class MessageController extends AdminController {

    protected $db;

    protected function _initialize()
    {
        parent::_initialize();
        $this->db = D('Home/Message');
    }


    public function index(){
        $pagesize = 10;
        $p = I('get.p',1);

        $count = $this->db->count();
        $data = $this->db->order('id desc')->page($p,$pagesize)->select();
        $page = new \Think\Page($count,$pagesize);
        $this->assign('list',$data);
        $this->assign('page',$page->show());
        $this->display();

    }

    public function detail($id){
        $info = $this->db->find($id);
        $this->assign('info',$info);
        $this->display();
    }

}