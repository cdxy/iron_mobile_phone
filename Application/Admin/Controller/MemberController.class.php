<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Admin\Controller;
use Common\Api\ImportExcelApi;
use Think\Upload;
use User\Api\UserApi;


/**
 * 管理前端注册用户
 * Class MemberController
 * @package Admin\Controller
 * Author: Mr.Li<LHB2002@qq.com>
 * Date: 2018-06-25 17:17
 */
class MemberController extends AdminController {

    private $model;
    private $ucm;

    protected function _initialize()
    {
        parent::_initialize();
        $this->model = D('Member');
        $this->ucm = D('UcenterMember');
    }


    public function index()
    {

        $map['M.status']  =   array('egt',0);
        $map['M.user_group'] = 1;
        if( $nickname = I('nickname')   ){
            $map['CM.mobile|M.nickname']=   array(intval($nickname),array('like','%'.$nickname.'%'),'_multi'=>true);
        }
        $Member = M('Member');
        $Member->alias('M')
               ->join('left join __UCENTER_MEMBER__ CM on CM.id = M.uid');
        $list   = $this->lists($Member, $map,'','M.*,CM.mobile');
        $grade=C('USER_GRADE');
        array_shift($grade);
        int_to_string($list,array(
            'status'=>array(
                1=>'正常',
                -1=>'删除',
                0=>'禁用',
                2=>'未审核',
                3=>'草稿'
            ),
            'grade'=>$grade
        ));
        $this->assign('_list', $list);
        $this->meta_title = '注册用户';
        $this->display();
    }

    public function edit($id){
        $Member = D('Member');
        if(IS_POST){
            $data = I('post.');
            $data = $Member->create($data,2);

            $status = $Member->where(array('uid'=>$id))->save($data);
            if(false!==$status){
                $this->success('保存成功');
            }
            else{
                $this->error('保存失败');
            }
        } else {
            $this->meta_title = '编辑用户';
            $info = $Member->alias('M')
                           ->join('__UCENTER_MEMBER__ UM on UM.id = M.uid')
                           ->field('M.*,UM.mobile')
                           ->where(array('M.uid'=>$id))
                           ->find();
            if(empty($info)){
                $this->error('没有找到用户');
            }
            $grade=C('USER_GRADE');
            array_shift($grade);

            $this->assign('info',$info);
            $this->assign('grade',$grade);
            $this->display();
        }
    }

    /**
     * 导入用户数据
     * Author: Mr.Li<LHB2002@qq.com>
     * Date: 2018-06-30 16:31
     */
    public function import(){
        if(IS_POST) {
            $file_driver = C('DOWNLOAD_UPLOAD');
            $Upload = new Upload($file_driver);
            $info = $Upload->upload();
            if (!$info) {
                $this->error($Upload->getError());
            }
            $file = $info['upload'];
            $path = $file_driver['rootPath'] . $file['savepath'] . $file['savename'];
            $import_api = new ImportExcelApi();
            $data = $import_api->_import(
                $path,
                $this->fieldName(),
                array($this, 'row_callback'),
                '',
                array(
                    'field' => 1,
                    'frist_data' => 2,
                    'max_row' => 3000,
                    'error_write' => true
                )
            );
            if($data){
                $this->assign('mes','导入成功');
            }
            else{
                $mes = $import_api->getError();
                $this->assign('mes',"导入成功{$mes['success']}条,错误{$mes['error_count']}条,<a href=\"{$mes['error_file']}\">错误详情(点击下载查看)</a>");
            }
        }
            $this->display();

    }

    private function fieldName(){
        //账号，密码，会员等级，有效期
        return array(
            'username'=>'用户名',
            'password'=>'密码',
            'grade'=>'等级',
        );
    }

    private $import_usernames = array();
    public function row_callback($data,&$error,$row){
        $username = $data['username'];
        if( $row_val = $this->existUsername($username) ){
            $error = '用户名已存在' . ($row_val===true?" 数据库":"表的".$row_val."行");
            return false;
        }
        $this->import_usernames[$username] = $row;

        $grade = $this->userGrade($data['grade']);
        $data = $this->ucm->create($data);
        if(!$data){
            $error = $this->ucm->getError();
            return false;
        }
        $id = $this->ucm->add($data);
        if(!$id){
            $error = $this->ucm->getError();
            return false;
        }
        //结构化数据
        $data['grade'] = $grade;
        $data['uid'] = $id;
        $data['user_group'] = 1;
        $this->model->add($data);
        return $data;
    }

    /**
     * 获取用户等级
     * Author: Mr.Li<LHB2002@qq.com>
     * Date: 2018-07-03 10:10
     */
    private function userGrade($grade){
        $grade_config=C('USER_GRADE');
        return array_search($grade,$grade_config);
    }

    /**
     * 检查用户名是否存在
     * @param $username
     * @return bool|mixed
     * Author: Mr.Li<LHB2002@qq.com>
     * Date: 2018-07-03 10:09
     */
    private function existUsername($username){
        if(array_key_exists($username,$this->import_usernames)){
            return $this->import_usernames[$username];
        }
        elseif ($this->ucm->where(array('username'=>$username))->find()){
            return true;
        }
        else{
            return false;
        }
    }
}