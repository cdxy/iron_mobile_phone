<?php
/**
 * User: Mr.Li <lhb2002@qq.com>
 * Date: 2016-4-1
 * Time: 21:06:22
 */
require_once('Wechat.php');
class WechatPay{

    const API_URL_PREFIX ='https://api.mch.weixin.qq.com';
    //统一下单
    const UNIFIED_ORDER_URL='/pay/unifiedorder';
    //查询订单
    const QUERY_ORDER_URL='/pay/orderquery';
    //关闭订单
    const CLOSE_ORDER_URL='/pay/closeorder';
    //申请退款
    const REFUND_URL='/secapi/pay/refund';
    //查询退款
    const QUERY_REFUND_URL='/pay/refundquery';
    //下载对账单
    const DOWNLOAD_BILL_URL='/pay/downloadbill';

    //沙箱模式地址
    const GETSIGN_KEY_URL='/pay/getsignkey';

    private $debug = true;
    private $appid;
    private $mch_id;
    private $key;
    private $appsecret;
    private $sslcert_path;
    private $sslkey_path;
    private $ssllca_path;

    private $wechat;
    private $error;
    /**
     * 构造方法
     */
    public function __construct($options=array())
    {
        $this->wechat = new Wechat($options);
        $this->appid = isset($options['appid'])?$options['appid']:'';
        $this->appsecret = isset($options['appsecret'])?$options['appsecret']:'';
        $this->mch_id = isset($options['mch_id'])?$options['mch_id']:'';
        $this->key = isset($options['key'])?$options['key']:false;
        $this->sslcert_path = isset($options['sslcert_path'])?$options['sslcert_path']:false;
        $this->sslkey_path = isset($options['sslkey_path'])?$options['sslkey_path']:false;
        $this->ssllca_path = isset($options['ssllca_path'])?$options['ssllca_path']:false;

        //todo 如果是沙箱模式,自动获取sandbox_signkey
        if($options['sandbox']){
            $this->getSignKey();
        }
    }

    /**
     * 获取沙箱sign
     */
    public function getSignKey(){
        $params['mch_id']         = $this->mch_id;
        $params['nonce_str']      = $this->wechat->generateNonceStr(16);
        $params['sign']           = self::_getOrderMd5($params);
        $data = $this->wechat->xml_encode($params);
        $data = $this->wechat->http_post(self::API_URL_PREFIX.self::GETSIGN_KEY_URL, $data);
        $sign_data = self::parsePayRequest($data,$this->wechat->errMsg,false,false);
        if($sign_data){
            $this->key = $sign_data['sandbox_signkey'];
            $this->log( 'getSignKey:'.$this->key );
        }
        else{
            $this->log("getSignKey ERROR".$this->error);
        }
    }

    /**
     * 使用证书
     */
    public function useCert(){
        //设置请求证书
        $this->wechat->setCert($this->sslcert_path,$this->sslkey_path,$this->ssllca_path);
    }
    /**
     * 统一下单接口生成支付请求
     * @param  $openid      string  用户OPENID相对于当前公众号
     * @param  $body        string  商品描述 少于127字节
     * @param  $orderId     string  系统中唯一订单号
     * @param  $money       integer 支付金额
     * @param  $notify_url  string  通知URL
     * @param  $trade_type  string 支付模式 JSAPI:公众号支付，NATIVE:扫码支付，APP:应用支付
     * @param  $extend      array|string   扩展参数
     * @return json|boolean json 直接可赋给JSAPI接口使用，boolean错误
     */
    public function unifiedOrder($openid, $body, $orderId, $money, $notify_url = '',$trade_type='JSAPI', $extend = array()) {
        if (strlen($body) > 127) $body = substr($body, 0, 127);
        $params = array(
            'openid'           => $openid,
            'appid'            => $this->appid,
            'mch_id'           => $this->mch_id,
            'nonce_str'        => $this->wechat->generateNonceStr(16),
            'body'             => $body ,
            'out_trade_no'     => $orderId,
            'total_fee'        => $money * 100, // 转换成分
            'spbill_create_ip' => get_client_ip(),
            'notify_url'       => $notify_url,
            'trade_type'       => $trade_type,
        );

        if('JSAPI'==$params['trade_type'] && empty($params['openid'])){
            $this->error = '必选具备openid参数';
            return false;

        }
        //dump($params);
        if (is_string($extend)) {
            $params['attach']  = $extend;
        } elseif (is_array($extend) && !empty($extend)) {
            $params = array_merge($params, $extend);
        }
        // 生成签名
        $params['sign'] = self::_getOrderMd5($params);
        $data=$this->wechat->xml_encode($params);
        $data=$this->wechat->http_post(self::API_URL_PREFIX.self::UNIFIED_ORDER_URL,$data);
        return self::parsePayRequest($data,$this->wechat->errMsg,true,true);
    }

    /**
     * 生成支付参数
     */
    public function createPayParams($prepay_id) {
        if (empty($prepay_id)) {
            $this->error = 'prepay_id参数错误';
            return false;
        }
        $params['appId']     = $this->appid;
        $params['timeStamp'] = (string)NOW_TIME;
        $params['nonceStr']  = $this->wechat->generateNonceStr(16);
        $params['package']   = 'prepay_id='.$prepay_id;
        $params['signType']  = 'MD5';
        $params['paySign']   = self::_getOrderMd5($params);
        //新版微信js timestamp 必须是小写，sdk中必须是大写
        $params['timestamp']= $params['timeStamp'];
        unset($params['timeStamp']);
        return $params;
    }

    /**
     * 查询订单
     */
    public function getOrderInfo($orderId, $type = 0) {
        $params['appid']          = $this->appid;
        $params['mch_id']         = $this->mch_id;
        if ($type == 1) {
            $params['transaction_id'] = $orderId;
        } else {
            $params['out_trade_no']   = $orderId;
        }
        $params['nonce_str']      = $this->wechat->generateNonceStr(16);
        $params['sign']           = self::_getOrderMd5($params);
        $data = $this->wechat->xml_encode($params);
        $data = $this->wechat->http_post(self::API_URL_PREFIX.self::QUERY_ORDER_URL, $data);
        return self::parsePayRequest($data,$this->wechat->errMsg,true,true);
    }

    /**
     * 关闭订单
     */
    public function closeOrder($orderId) {
        $params['appid']          = $this->appid;
        $params['mch_id']         = $this->mch_id;
        $params['out_trade_no']   = $orderId;
        $params['nonce_str']      = $this->wechat->generateNonceStr(16);
        $params['sign']           = self::_getOrderMd5($params);
        $data = $this->wechat->xml_encode($params);
        $data = $this->wechat->http_post(self::API_URL_PREFIX.self::CLOSE_ORDER_URL, $data);
        return self::parsePayRequest($data,$this->wechat->errMsg,true,true);
    }

    /**
     * 申请退款 需要证书操作
     */
    public function refundOrder($orderId, $refundId, $total_fee, $refund_fee = 0) {
        $this->log('refundOrder');
        if(!$refund_fee){ //默认参数时，退换所有金额
            $refund_fee = $total_fee;
        }
        $params['appid']          = $this->appid;
        $params['mch_id']         = $this->mch_id;
        $params['nonce_str']      = $this->wechat->generateNonceStr(16);
        $params['out_trade_no']   = $orderId;
        $params['out_refund_no']  = $refundId;
        $params['total_fee']      = $total_fee * 100;
        $params['refund_fee']     = $refund_fee * 100;
        $params['op_user_id']     = $this->mch_id;
        $params['sign']            = self::_getOrderMd5($params);

        $this->useCert();
        $data = $this->wechat->xml_encode($params);
        $data = $this->wechat->http_post(self::API_URL_PREFIX.self::REFUND_URL, $data,false,true);
        return self::parsePayRequest($data,$this->wechat->errMsg,true,true);
    }

    /**
     * 获取退款状态
     */
    public function getRefundStatus($orderId) {
        $params['appid']          = $this->appid;
        $params['mch_id']         = $this->mch_id;
        $params['nonce_str']      = $this->wechat->generateNonceStr(16);
        $params['out_trade_no']   = $orderId;
        $params['sign']           = self::_getOrderMd5($params);
        $data = $this->wechat->xml_encode($params);
        $data =$this->wechat->http_post(self::API_URL_PREFIX.self::QUERY_REFUND_URL, $data);
        return self::parsePayRequest($data,$this->wechat->errMsg,true,true);
    }

    /**
     * XML文档解析成数组，并将键值转成小写
     * @param  xml $xml
     * @return array
     */
    private function _extractXml($xml) {
        $data = (array)simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
        return array_change_key_case($data, CASE_LOWER);
    }

    /**
     * 本地MD5签名
     */
    private function _getOrderMd5($params,$key='') {
        if(empty($key)){
            $key = $this->key;
        }
        return $this->wechat->getSignature($params,'md5','&key='.$key);
    }

    /**
     * 接收数据签名校验
     */
    private function _checkSign($data) {
        $sign = $data['sign'];
        unset($data['sign']);
        if (strtoupper(self::_getOrderMd5($data)) != strtoupper($sign)) {
            $this->error = '签名校验失败';
            return false;
        } else {
            return true;
        }
    }

    public function getError(){
        return $this->error;
    }


    /**
     * 解析请求结果
     * @param $data 服务器返回的元素结构
     * @param $mes 请求的网络通信错误消息
     * @param bool|true $result 是否为一个结果集
     * @param bool|false $checkSign 是否需要进行sign验证
     * @return array|bool
     */
    public function parsePayRequest($data, $mes, $result =true, $checkSign = false) {
        if(false===$data){
            $this->error = '网络请求失败:' . $mes;
            return false;
        }
        $this->log('parsePayRequest xml');
        $this->log($data);
        $data = self::_extractXml($data);
        if (empty($data)) {
            $this->error = '支付返回内容解析失败';
            return false;
        }
        $this->log('parsePayRequest');
        $this->log($data);
        // 有返回结果 并且是SUCCESS的时候
        if ($data['return_code'] != 'SUCCESS') {
            $this->error = $data['return_msg'];
            return false;
        }

        //结果模式时，处理状态不成功
        if ($result && $data['result_code'] != 'SUCCESS') {
            $this->error = $data['err_code'] .":". $data['err_code_des'];
            return false;
        }

        //不需要验证 或验证通过时直接返回值
        if (!$checkSign || $check_sign = self::_checkSign($data)) {
            return $data;
        }
        else{
            //验证失败
            $this->error = $data['err_code'] .":". $data['err_code_des'];
            return false;
        }

    }

    /**
     * 接口通知接收
     */
    public function getNotify() {
        if(isset($GLOBALS["HTTP_RAW_POST_DATA"])){
            $data = $GLOBALS["HTTP_RAW_POST_DATA"];
            return self::parsePayRequest($data);
        }
        else{
            $this->error = '没有获取到内容';
            return false;
        }

    }

    /**
     * 对支付回调接口返回成功通知
     */
    public function returnNotify($return_msg = true) {
        if ($return_msg == true) {
            $data = array(
                'return_code' => 'SUCCESS',
            );
        } else {
            $data = array(
                'return_code' => 'FAIL',
                'return_msg'  => $return_msg
            );
        }
        exit(self::array2xml($data));
    }

    public function array2xml($array,$encoding='utf-8') {
        $xml='<?xml version="1.0" encoding="'.$encoding.'"?>';
        $xml.='<xml>';
        $xml.=$this->_array2xml($array);
        $xml.='</xml>';
        return $xml;
    }

    private function _array2xml($array)
    {
        $xml='';
        foreach($array as $key=>$val){
            if(is_numeric($key)){
                $key="item id=\"$key\"";
            }else{
                //去掉空格，只取空格之前文字为key
                list($key,)=explode(' ',$key);
            }
            $xml.="<$key>";
            $xml.=is_array($val)?$this->_array2xml($val):$val;
            //去掉空格，只取空格之前文字为key
            list($key,)=explode(' ',$key);
            $xml.="</$key>";
        }
        return $xml;
    }

    protected function log($log){
        if($this->debug){
            $log = is_string($log) ? $log : json_encode($log);
            error_log($_SERVER['REMOTE_ADDR'] . ' ' . $_SERVER['REQUEST_URI']  .' '. date('y-m-d h:i:s',time()). "\r\n{$log}\r\n" ,3,'test.log');
        }
    }
}