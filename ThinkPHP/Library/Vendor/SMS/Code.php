<?php
/**
 * Author: Mr.Li<LHB2002@qq.com>
 * Date: 2018-06-30 11:39
 */
namespace lhb\SMS;
class Code
{
    private $config = array(
        'userid'=>'1402',
        'account'=>'yjlsswg',
        'password'=>'e3f5y7k9q1'
    );

    private $error='';

    const MAIN_URL = 'http://dc.28inter.com/sms.aspx';

    public function config($config){
        $this->config = $config;
    }

    /**
     * 结构化数据
     * @param $action
     * @param $data
     * @return array
     * Author: Mr.Li<LHB2002@qq.com>
     * Date: 2018-06-30 14:30
     */
    protected function structure( $data){
        $data = array_merge($this->config,$data);
        return $data;
    }

    /**
     * 发送post请求
     * @param $url
     * @param $data
     * @return mixed
     * @throws NetException
     * @throws XMLException
     * Author: Mr.Li<LHB2002@qq.com>
     * Date: 2018-06-30 14:30
     */
    public function post($data){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL,self::MAIN_URL);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        return $result;

    }

    /**
     * 解析返回值
     * @param $data
     * @return mixed
     * @throws XMLException
     * Author: Mr.Li<LHB2002@qq.com>
     * Date: 2018-06-30 14:30
     */
    protected function parseXML($data){
        if(!$data){
            throw new SMSException('网络请求错误');
        }
        try{
            $xml=simplexml_load_string($data);
            $result =  json_decode(json_encode($xml),TRUE);
        }
        catch (\Exception $e){
            throw new SMSException('XML解析错误');
        }

        if( 'Faild'== $result['returnstatus'] ){
            throw new SMSException($result['message']);
        }
        unset( $result['returnstatus'] );
        return $result;
    }

    /**
     * 请求接口
     * @param $data
     * @return mixed
     * @throws Exception
     * Author: Mr.Li<LHB2002@qq.com>
     * Date: 2018-06-30 14:34
     */
    public function request($data){
        try{
            $data = $this->structure($data);
            $result = $this->post($data);
            return $this->parseXML($result);
        }
        catch (SMSException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    /**
     * 发送短信
     * @param $mobile
     * @param $content
     * @param string $sendTime
     * Author: Mr.Li<LHB2002@qq.com>
     * Date: 2018-06-30 14:30
     */
    public function send($mobile, $content, $sendTime=''){
        $data = array(
            'action'=>'send',
            'mobile' => $mobile,
            'content' => $content,
            'sendTime' => $sendTime,
            'extno'=>''
        );
        return $this->request($data);
    }

    /**
     * 查询余额
     * Author: Mr.Li<LHB2002@qq.com>
     * Date: 2018-06-30 14:41
     */
    public function overage(){
        $data = array(
            'action'=>'overage'
        );
        return $this->request($data);
    }

    /**
     * 检查发送关键词是否违法
     * @param $content
     * @return mixed
     * @throws Exception
     * Author: Mr.Li<LHB2002@qq.com>
     * Date: 2018-06-30 14:47
     */
    public function checkkeyword($content ){
        $data = array(
            'action'=>'checkkeyword',
            'content'=>$content
        );
        return $this->request($data);
    }

    public function getError(){
        return $this->error;
    }
}